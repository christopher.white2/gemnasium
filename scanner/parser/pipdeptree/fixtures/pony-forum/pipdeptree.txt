bleach==1.4
  - html5lib [required: >=0.999, installed: 1.0.1]
    - six [required: >=1.9, installed: 1.11.0]
    - webencodings [required: Any, installed: 0.5.1]
  - six [required: Any, installed: 1.11.0]
coverage==4.4.2
django-debug-toolbar==0.11
  - django [required: >=1.4.2,<1.7, installed: 1.4.12]
  - sqlparse [required: Any, installed: 0.2.4]
django-nose==1.2
  - Django [required: >=1.2, installed: 1.4.12]
  - nose [required: >=1.2.1, installed: 1.3.7]
django-registration==0.8
django-secure==1.0
  - Django [required: >=1.4.2, installed: 1.4.12]
docutils==0.11
Markdown==2.4
pipdeptree==0.10.1
  - pip [required: >=6.0.0, installed: 9.0.1]
psycopg2==2.7.3.2
setuptools==38.2.4
smartypants==1.8.3
South==0.8.4
virtualenv==15.1.0
wheel==0.30.0
